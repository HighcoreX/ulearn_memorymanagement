﻿using System;
using System.Diagnostics;

namespace uLearn_MemoryManagement
{
    public class Timer
    {
        private TimeSpan elapsed;
        public int ElapsedMilliseconds
        {
            get { return elapsed.Milliseconds; }
        }
        public long ElapsedTicks
        {
            get { return elapsed.Ticks; }
        }
        public Process Start()
        {
            elapsed = TimeSpan.Zero;
            return new Process(this);
        }
        public Process Continue()
        {
            return new Process(this);
        }
        public class Process : IDisposable
        {
            readonly Timer timer;
            readonly Stopwatch stopWatch;
            public Process(Timer timer)
            {
                this.timer = timer;
                stopWatch = new Stopwatch();
                stopWatch.Start();
            }
            public void Dispose()
            {
                timer.elapsed += stopWatch.Elapsed;
                stopWatch.Stop();
            }
        }
    }
}
