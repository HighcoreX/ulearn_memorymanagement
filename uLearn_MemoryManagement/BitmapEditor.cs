﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;

namespace uLearn_MemoryManagement
{
    public class BitmapEditor : IDisposable
    {
        private Bitmap bitmap;
        private BitmapData bitmapData;
        private IntPtr ptr;
        private byte[] pixels;
        public BitmapEditor(Bitmap sourceBitmap)
        {
            bitmap = sourceBitmap;
            Rectangle rectangle = new Rectangle(0, 0, bitmap.Width, bitmap.Height);
            bitmapData = bitmap.LockBits(rectangle, System.Drawing.Imaging.ImageLockMode.ReadWrite, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            pixels = new byte[(bitmap.Width * bitmap.Height) * 3];
            ptr = bitmapData.Scan0;
            Marshal.Copy(ptr, pixels, 0, pixels.Length);
        }
        public void Dispose() 
        {
            Marshal.Copy(pixels, 0, ptr, pixels.Length);
            bitmap.UnlockBits(bitmapData);
        }
        public void SetPixel(int x, int y, byte colorR, byte colorG, byte colorB)
        {
            int i = ((y * bitmap.Width) + x) * 3;
            pixels[i] = colorB;
            pixels[i + 1] = colorG;
            pixels[i + 2] = colorR;
        }
        public Color GetPixel(int x, int y)
        {
            int i = ((y * bitmap.Width) + x) * 3;
            return Color.FromArgb(pixels[i + 2], pixels[i + 1], pixels[i]);
        }
    }
}