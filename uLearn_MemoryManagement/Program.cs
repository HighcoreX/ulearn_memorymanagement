﻿using System;
using System.Threading;
using System.Drawing;
using System.IO;

namespace uLearn_MemoryManagement
{
    class Program
    {
        static void Main(string[] args)
        {
            Bitmap bitmap;
            try
            {
                bitmap = (Bitmap)Bitmap.FromFile("sailor_doom.jpg");
            }
            catch (FileNotFoundException e)
            {
                Console.WriteLine(e);
                Console.ReadKey();
                return;
            }

            Timer timer = new Timer();

            Console.WriteLine("Bitmap.GetPixel x 10000:");
            using (timer.Start())
            {
                for (byte x = 0; x < 100; x++)
                {
                    for (byte y = 0; y < 100; y++ )
                    {
                        bitmap.GetPixel(x, y);
                    }
                }     
            }
            Console.WriteLine("time: {0} msec {1} ticks", timer.ElapsedMilliseconds, timer.ElapsedTicks);

            Console.WriteLine("\nBitmapEditor.GetPixel x 10000:");
            using (timer.Start())
            {
                using (var bitmapEditor = new BitmapEditor(bitmap))
                {
                    for (byte x = 0; x < 100; x++)
                    {
                        for (byte y = 0; y < 100; y++)
                        {
                            bitmapEditor.GetPixel(x, y);
                        }
                    }
                }
            }
            Console.WriteLine("time: {0} msec {1} ticks", timer.ElapsedMilliseconds, timer.ElapsedTicks);

            Console.WriteLine("\nBitmap.SetPixel x 10000:");
            using (timer.Start())
            {
                for (byte x = 0; x < 100; x++)
                {
                    for (byte y = 0; y < 100; y++)
                    {
                        bitmap.SetPixel(x, y, Color.White);
                    }
                }
            }
            bitmap.Save("sailor_doom1.jpg");
            Console.WriteLine("time: {0} msec {1} ticks", timer.ElapsedMilliseconds, timer.ElapsedTicks);

            Console.WriteLine("\nBitmapEditor.SetPixel x 10000:");
            using (timer.Start())
            {
                using (var bitmapEditor = new BitmapEditor(bitmap))
                {
                    for (byte x = 0; x < 100; x++)
                    {
                        for (byte y = 0; y < 100; y++)
                        {
                            bitmapEditor.SetPixel(x, y, 255, 255, 255);
                        }
                    }
                }
            }
            bitmap.Save("sailor_doom2.jpg");
            Console.WriteLine("time: {0} msec {1} ticks", timer.ElapsedMilliseconds, timer.ElapsedTicks);

            bitmap.Dispose();
            Console.ReadKey();
        }
    }
}
